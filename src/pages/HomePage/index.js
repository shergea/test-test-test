import React, { useContext, useEffect } from "react";
//import { Table } from "react-bootstrap";
import { Column, Table, WindowScroller, AutoSizer } from "react-virtualized";
import DATA from "./bulten_data.json";
import { MainContext } from "../../store";
import { addBet, removeBet, updateBet } from "./actions";

const { Events } = DATA;
const EventList = Object.values(Events);

// eslint-disable-next-line
const CellClickFunc = (dispatch, bet, cellData, dataKey, rowData) => () => {
  if (!bet) {
    dispatch(
      addBet({
        C: rowData.C,
        N: rowData.N,
        Rate: cellData,
        Selection: dataKey
      })
    );
  } else if (bet && bet.Selection !== dataKey) {
    dispatch(
      updateBet({
        C: rowData.C,
        N: rowData.N,
        Rate: cellData,
        Selection: dataKey
      })
    );
  } else if (bet && bet.Selection === dataKey) {
    dispatch(
      removeBet({
        C: rowData.C
      })
    );
  }
};

const CellRenderer = (state, dispatch) => ({
  cellData,
  columnData,
  columnIndex,
  dataKey,
  isScrolling,
  rowData,
  rowIndex
}) => {
  let bet = state[state.findIndex(obj => obj.C === rowData.C)];
  return (
    <div
      style={
        bet && bet.Selection === dataKey
          ? { backgroundColor: "yellow", cursor: "pointer" }
          : { cursor: "pointer" }
      }
      onClick={CellClickFunc(dispatch, bet, cellData, dataKey, rowData)}
    >
      {cellData}
    </div>
  );
};

const MatchNameCellRenderer = ({
  cellData,
  columnData,
  columnIndex,
  dataKey,
  isScrolling,
  rowData,
  rowIndex
}) => {
  return <div style={{ color: "red", fontWeight: "bold" }}>{cellData}</div>;
};

const HomePage = () => {
  const cellWidth = 40;
  const { state, dispatch } = useContext(MainContext);
  let totalRate = 1;
  state.forEach(item => {
    totalRate *= item.Rate;
  });
  return (
    <>
      <WindowScroller>
        {({ height, isScrolling, onChildScroll, scrollTop }) => (
          <AutoSizer>
            {({ width }) => (
              <Table
                width={width}
                autoHeight
                height={height}
                isScrolling={isScrolling}
                onScroll={onChildScroll}
                scrollTop={scrollTop}
                headerHeight={50}
                rowHeight={50}
                rowCount={EventList.length}
                rowGetter={({ index }) => EventList[index]}
              >
                <Column
                  label="Event Count:@@@@@@"
                  dataKey="C"
                  width={300}
                  cellRenderer={MatchNameCellRenderer}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return (
                      rowData["C"] + " " + rowData["T"] + " " + rowData["N"]
                    );
                  }}
                />
                <Column
                  label="Yorumlar"
                  dataKey="C"
                  width={100}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "Yorumlar";
                  }}
                />
                <Column
                  label=""
                  dataKey="MSB"
                  width={20}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "4";
                  }}
                />
                <Column
                  label="1"
                  dataKey="1"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["1"].OC["0"].O;
                  }}
                />
                <Column
                  label="X"
                  dataKey="X"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["1"].OC["1"].O;
                  }}
                />
                <Column
                  label="2"
                  dataKey="2"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="Alt"
                  dataKey="Alt"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["5"].OC["25"]["O"];
                  }}
                />
                <Column
                  label="Üst"
                  dataKey="Üst"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["5"].OC["26"]["O"];
                  }}
                />
                <Column
                  label="H1"
                  dataKey="H1"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="1"
                  dataKey="1"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="X"
                  dataKey="X"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="2"
                  dataKey="2"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="H2"
                  dataKey="H2"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="1-X"
                  dataKey="1-X"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["2"].OC["3"]["O"];
                  }}
                />
                <Column
                  label="1-2"
                  dataKey="1-2"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["2"].OC["4"]["O"];
                  }}
                />
                <Column
                  label="X-2"
                  dataKey="X-2"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return rowData.OCG["2"].OC["5"]["O"];
                  }}
                />
                <Column
                  label="Var"
                  dataKey="Var"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
                <Column
                  label="Yok"
                  dataKey="Yok"
                  width={cellWidth}
                  cellRenderer={CellRenderer(state, dispatch)}
                  cellDataGetter={({ columnData, dataKey, rowData }) => {
                    return "";
                  }}
                />
              </Table>
            )}
          </AutoSizer>
        )}
      </WindowScroller>
      <div
        style={{
          position: "fixed",
          bottom: 0,
          right: 0,
          minHeight: 80,
          width: 500,
          backgroundColor: "grey",
          border: "5px solid black"
        }}
      >
        <ul style={{ listStyle: "none", maxHeight: 200, overflowY: "auto" }}>
          {state.map(item => (
            <li>
              <span>
                Kod:{item.C} Maç:{item.N} Oran:{item.Rate}
              </span>
              <hr />
            </li>
          ))}
        </ul>
        <span style={{ fontSize: 26, fontWeight: "bold" }}>
          TOPLAM ORAN: {state.length > 0 ? totalRate.toFixed(3) : 0}
        </span>
      </div>
    </>
  );
};

export default HomePage;
