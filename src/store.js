import React, { createContext, useReducer } from "react";
import { node } from "prop-types";
// load reducers
import couponReducer from "./reducers/couponReducer";
// craete initial payload
const coupon = [];
// create context
export const MainContext = createContext(coupon);
// create Store
function Store({ children }) {
  const [state, dispatch] = useReducer(couponReducer, coupon);
  const value = { state, dispatch };
  return <MainContext.Provider value={value}>{children}</MainContext.Provider>;
}
Store.defaultProps = {
  children: null
};
Store.propTypes = {
  children: node
};
export default Store;
